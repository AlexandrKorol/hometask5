import Full from './FullToDoList';
import Incomplete from './Incomplete';
import Complete from './Complete';
export default [
    {
        path: '/',
        exact: true,
        component: Full
    },
    {
        path: '/incomplete',
        exact: true,
        component: Incomplete
    },
    {
        path: '/complete',
        exact: true,
        component: Complete
    }
]