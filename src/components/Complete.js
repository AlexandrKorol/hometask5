import React from 'react';
import { connect } from 'react-redux';
import checkmark from '../assets/icons8-checkmark-24.png';
const Complete = ({ toDoList }) => {

    return (
        <ul>
            {
                toDoList.map((item, id) => {
                    console.log("This is item", item, " and current id: ", item.id);
                    return item.done ?
                        <li id={item.id} key={id} className="task-completed">
                            <img src={checkmark} alt="myimage"></img>
                            {item.toDoItem}
                        </li>
                        :
                        null
                })
            }
        </ul>
    )
}

const mapStateToProps = (state) => {
    return {
        toDoList: state.items
    }
};

export default connect(mapStateToProps)(Complete);