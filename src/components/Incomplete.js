import React from 'react';
import { connect } from 'react-redux';
import pin from '../assets/icons8-pin-24.png';
const Incomplete = ({ toDoList }) => {

    return (
        <div>
            <ul>
                {
                    toDoList.map((item, id) => {
                        console.log("This is item", item, " and current id: ", item.id);
                        return !item.done ?
                            <li id={item.id} key={id}>
                                <img src={pin} alt="myimage"></img>
                                {item.toDoItem}

                            </li> :
                            null


                    })
                }
            </ul>
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        toDoList: state.items
    }
};

export default connect(mapStateToProps)(Incomplete);