import React from 'react';
import '../index.css'
import { BrowserRouter, NavLink, Switch, Route } from 'react-router-dom';
import rootRoutes from './rootRoutes';

const App = () => {


  return (
    <BrowserRouter>
      <div className="App">
        <nav>
          <NavLink to="/">All tasks</NavLink>
          <NavLink to="/incomplete">Incomplete tasks</NavLink>
          <NavLink to="/complete">Complete tasks</NavLink>
        </nav>

        <Switch>
          {
            rootRoutes.map(item => (
              <Route {...item} />
            ))
          }
        </Switch>


      </div>
    </BrowserRouter>
  );
}





export default App;

