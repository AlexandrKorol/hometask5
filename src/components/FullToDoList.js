import React from 'react';
import checkmark from '../assets/icons8-checkmark-24.png';
import pin from '../assets/icons8-pin-24.png';
import { connect } from 'react-redux';

class FullList extends React.Component {
    
    state = {
        newItem: '',
        index: 0 // счетчик
    }
    setItemToState = (e) => {
        this.setState({
            newItem: e.target.value,
        });
    }
    addToDoItem = () => {
        this.setState({
            index: this.state.index + 1
        }) // кожен наступний елемент має індекс + 1
        const { addToDoItem } = this.props;
        addToDoItem(this.state.newItem, this.state.index);
    }
    removeItem = (id) => () => {
        const { removeToDoItem, toDoList } = this.props;
        const payload = toDoList.filter(toDoItem => {
            return toDoItem.id !== id;
        })
        removeToDoItem(payload);
    }
    markAsDone = (id) => () => {
        const { completeTask, toDoList } = this.props;
        const payload = toDoList.map(toDoItem => {
            if (toDoItem.id === id) {
                return {
                    ...toDoItem,
                    done: !toDoItem.done
                }
            }
            else {
                return toDoItem
            }
        });
        completeTask(payload);
    }
    render() {
        const { addToDoItem, setItemToState, removeItem, markAsDone } = this;
        const { toDoList } = this.props;
        return (
            <div>
                <div className="shapka">
                    <h2>Enter here what to do</h2>
                    <input placeholder="Enter todo" onChange={setItemToState} />
                    <button onClick={addToDoItem}>Add</button>
                </div>

                <ul>
                    {
                        toDoList.map((item, id) => {
                            console.log("This is item", item, " and current id: ", item.id);
                            return !item.done ?
                                <li id={item.id} key={id}>
                                    <img src={pin} alt="myimage"></img>
                                    {item.toDoItem}
                                    <button onClick={removeItem(item.id)} className="redBtn">Remove item</button>
                                    <button onClick={markAsDone(item.id)} className="greenBtn">Complete</button>
                                </li> :
                                <li id={item.id} key={id} className="task-completed">
                                    <img src={checkmark} alt="myimage"></img>
                                    {item.toDoItem}
                                    <button onClick={removeItem(item.id)} className="redBtn">Remove item</button>
                                    <button onClick={markAsDone(item.id)} className="greenBtn">Complete</button>
                                </li>
                        })
                    }
                </ul>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        toDoList: state.items
    }
};

const mapDispatchToProps = (dispatch) => ({
    addToDoItem: (todo, index) => {
        dispatch({
            type: 'ADD_ITEM',
            payload: {
                id: index,
                toDoItem: todo,
                done: false
            }
        })
    },
    removeToDoItem: (todo) => {
        dispatch({
            type: "REMOVE_ITEM",
            payload: todo
        })
    },
    completeTask: (todo) => {
        dispatch({
            type: "COMPLETE_ITEM",
            payload: todo
        })
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(FullList);