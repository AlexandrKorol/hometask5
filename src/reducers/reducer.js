const initialState ={
    items: [],
    completeItems:[]
}

const reducer = (state = initialState, action) => {
    switch( action.type ){
        case "ADD_ITEM":
            return{
                ...state,
                items: [...state.items, action.payload]
            }
        case "REMOVE_ITEM":
            
            return{
                ...state,
                items: action.payload
            }
        case "COMPLETE_ITEM":
            return{
                ...state,
                items: action.payload
            }
        
        default:
            return state
    }
}
export default reducer;